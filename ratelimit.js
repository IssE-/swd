
// *********************************
// *** Dynamic rate limit search ***
// *********************************

// Does calls to given url.
// Continues even after we reach the rate limit
// After the limit resets, we will increase the dynamicly interval.
// when we have found a stable interval, we will end the excecution.


var http = require('http');

    // *** Possibility to  expand ***
// Remove hard-coded values.
// Add sometype of interface that takes input.
var callUrl = 'http://13.93.106.105:8080/api/hello';
var interval = 600;

var hits = 0;
var miss = 0;
var mostHits = 0;
var limitReached = false;

function getCallBasic(url) {
    console.log('trying with interval: ' + interval)
    http.get( url, (resp) => {

        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // Handle the data we recived.
        resp.on('end', () => {

                // *** Possibility to  expand ***
            // Instead of checking the response string
            // Should be checking the status code

            // successful call to api.
            if (data == "Hello from SWD!") {
                
                // The limit has been reseted as we have had misses in last call
                if(miss > 0)
                {
                        // *** Possibility to  expand ***
                    // Instead of using milliseconds in calculations, should be using timestamps
                    // Values using milliseconds might get too large to handle if rate limit resets slower


                    // Calculate the time we were over the limit
                    var missTime = interval * miss;

                    // Distribute evenly the time we were over limit, into the successful intervals.
                    // -> Longer intervals. 
                    var newInterval = (missTime / hits) + interval;
                    interval = newInterval;
                   
                    mostHits = hits;
                    // reset the hits and misses. Now we have a new interval
                    // we hope it will go through without reaching limit.
                    miss = 0;
                    hits = 0;
                }

                // if we get more hits after lowering interval 
                // -> we have a stable interval, we wont ever reach the limit.
                if(limitReached && hits > mostHits)
                {

                    console.log("We have found a stable interval!");
                    console.log("It is: " + interval +" milliseconds between calls.");
                    console.log("We can stop calibration process.");

                        // *** Possibility to  expand ***
                    // In a real system we could save the url and interval for later use.
                    // We can also start to lower the interval to get closer to the limit.

                    process.exit();
                }
                ++hits;
                console.log("Calls without reaching limit: " + hits);

            }
            else { 
                // We have reached the rate limit.
                ++miss;
                limitReached = true;

                
                    // *** Possibility to  expand ***
                // If this was a real system, we should remember the last call we tried to make.
                // We don't want to miss any information because we reached limit
                // then use the same call until we get through

                console.log("Limit reached, reseting counter");
            }
            
        
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
    
    // infinitly loop through the same call.
    // Interval will change if we reach limit
    setTimeout(getCallBasic, interval, callUrl);
}

setTimeout(getCallBasic, interval, callUrl);
